/**
 * @file
 * Google map block view JS.
 */

jQuery(document).ready(function ($) {

    initializeMap();
    function initializeMap() {
            /*
             * create map.
             */
            var map = new google.maps.Map(document.getElementById("google_map_field_block"), {
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            /*
             * create infowindow (which will be used by markers).
             */
            var infoWindow = new google.maps.InfoWindow();

            /*
             * marker creater function (acts as a closure for html parameter)
             */
            function createMarker(options, html) {
                var marker = new google.maps.Marker(options);
                if (html) {
                    google.maps.event.addListener(marker, "click", function () {
                        infoWindow.setContent(html);
                        infoWindow.open(options.map, this);
                    });
                }
                return marker;
            }

            /*
             * add markers to map.
             */

            var markers = [];
            var points = Drupal.settings.google_map_field_block.points;
            var bounds = new google.maps.LatLngBounds();

            for (var i = 0; i < points.length; i++) {
                        var lat_lang = new google.maps.LatLng(points[i].lat_lang[0], points[i].lat_lang[1]);
                        circleIcon = {
                                        path: google.maps.SymbolPath.CIRCLE,
                                        fillColor: "#000",
                                        fillOpacity: 1,
                                        scale: 2,
                                        strokeColor: points[i].color,
                                        strokeWeight: 10
                                    };
                        var marker = createMarker({
                            position: lat_lang,
                            map: map,
                            icon: circleIcon
                        }, "<h1>" + points[i].content + "</h1>");
                        markers.push(marker);
                        bounds.extend(lat_lang);
                }
                map.fitBounds(bounds);

            if (Drupal.settings.google_map_field_block.cluster != 0) {
              var markerCluster = new MarkerClusterer(map, markers);
            }

    }
});
