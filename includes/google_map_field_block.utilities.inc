<?php
/**
 * @file
 * All utility functions declared in this file.
 */

/**
 * A function to get content types that have google map fields.
 *
 * @return mixed
 *   array of google map fields
 */
function google_map_field_block_get_google_map_field_types() {

  $content_fields = google_map_field_block_get_content_types_gmf_field_array();

  $qualified_types = array();
  foreach ($content_fields as $type) {
    $type_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'];
    if (variable_get($type_key) == 1) {
      $qualified_types[] = $type['machine_name'];
    }
  }
  return $qualified_types;
}

/**
 * Description: A function to get all fields.
 *
 * @param string $type
 *   The content type.
 *
 * @return mixed
 *   An array of GM fields in content type.
 */
function google_map_field_block_get_google_map_fields_by_type($type) {
  $fields_array = google_map_field_block_get_gm_field_array_for_type($type);

  $qualified_fields = array();
  foreach ($fields_array['fields'] as $field) {
    $field_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type . "_" . $field['machine_name'];
    if (variable_get($field_key) == 1) {
      $qualified_fields[] = $field['machine_name'];
    }
  }
  return $qualified_fields;
}

/**
 * A function that will get you a array of Google map fields for type.
 *
 * @param string $type
 *   The machine_name of the content type.
 *
 * @return mixed
 *   An array of google map fields.
 */
function google_map_field_block_get_gm_field_array_for_type($type) {
  $passable_field_modules = explode(GOOGLE_MAP_FIELD_BLOCK_ARRAY_CONSTANT_DELIMETER, GOOGLE_MAP_FIELD_BLOCK_PASSABLE_FIELD_MODULES);
  $passable_field_types = explode(GOOGLE_MAP_FIELD_BLOCK_ARRAY_CONSTANT_DELIMETER, GOOGLE_MAP_FIELD_BLOCK_PASSABLE_FIELD_TYPES);
  $fields_array = array();
  // Get field list for content type.
  $field_list = field_info_instances('node', $type);
  $file_field_count = 0;
  foreach ($field_list as $field_key => $field_value) {
    if (in_array($field_value['widget']['module'], $passable_field_modules) && in_array($field_value['widget']['type'], $passable_field_types)) {
      $fields_array['fields'][$file_field_count]['label'] = $field_value['label'];
      $fields_array['fields'][$file_field_count]['machine_name'] = $field_key;
      $file_field_count++;
    }
  }

  return $fields_array;
}
