<?php
/**
 * @file
 * This file contains all admin config forms.
 */

/**
 * Implements hook_form().
 */
function google_map_field_block_configure_form($node, &$form_state) {
  $form = array();
  $form['overview'] = array(
    '#markup' => t('This interface allows the user to configure the dimensions
        and other aspects of the block and also select what content types and fields
        are shown in the block'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['google_map_field_block_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('google_map_field_block_api_key'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['google_map_field_block_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map Block Width'),
    '#default_value' => variable_get('google_map_field_block_width', 300),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number'),
  );
  $form['google_map_field_block_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Map Block Height'),
    '#default_value' => variable_get('google_map_field_block_height', 300),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number'),
  );

  $form['google_map_cluster'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable markers cluster for google map'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['google_map_cluster']['google_map_field_block_cluster_enabler'] = array(
    '#type' => 'checkbox',
    '#title' => '',
    '#default_value' => variable_get('google_map_field_block_cluster_enabler', 0),
  );
  $content_fields = google_map_field_block_get_content_types_gmf_field_array();
  foreach ($content_fields as $type) {
    // Set all the content types which are having file fields.
    $fieldset_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'] . "_fieldset";
    $fieldset_fields_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'] . "_fieldset_fields";
    $type_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'];
    $form[$fieldset_key] = array(
      '#type' => 'fieldset',
      '#title' => $type['label'],
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form[$fieldset_key][$type_key] = array(
      '#type' => 'checkbox',
      '#title' => $type['label'],
      '#default_value' => variable_get($type_key, 0),
      '#attributes' => array('class' => array('parent-type-checkbox')),
    );

    $form[$fieldset_key][$type_key . '_color'] = array(
      '#type' => 'textfield',
      '#title' => $type['label'] . " Color",
      '#description' => t('Will define the color of the marker on the map. Give the hezadecimal value Ex: #69A1F4'),
      '#default_value' => variable_get($type_key . '_color', '#69A1F4'),
      '#attributes' => array('class' => array('parent-type-checkbox')),
    );

    $form[$fieldset_key][$fieldset_fields_key] = array(
      '#type' => 'fieldset',
      '#title' => t('Select the fields which are in this content type.'),
      '#states' => array(
        'visible' => array(
          ':input[name=' . $type_key . ']' => array('checked' => TRUE),
        ),
      ),
    );
    // Set all the file fields.
    foreach ($type['fields'] as $field) {
      $field_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'] . "_" . $field['machine_name'];
      $form[$fieldset_key][$fieldset_fields_key][$field_key] = array(
        '#type' => 'checkbox',
        '#title' => $field['label'],
        '#default_value' => variable_get($field_key, 0),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Implements hook_validate().
 */
function google_map_field_block_configure_form_validate($form, &$form_state) {
  $content_fields = google_map_field_block_get_content_types_gmf_field_array();
  foreach ($content_fields as $type) {
    $fieldset_fields_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'] . "_fieldset_fields";
    $type_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'];

    if ($form_state['values'][$type_key] == 1) {
      $field_checked = FALSE;
      foreach ($type['fields'] as $field) {
        $field_key = GOOGLE_MAP_FIELD_BLOCK_FIELD_PREFIX . $type['machine_name'] . "_" . $field['machine_name'];
        if ($form_state['values'][$field_key] == 1) {
          $field_checked = TRUE;
          break;
        }
      }

      if (!$field_checked) {
        form_set_error($fieldset_fields_key, t('Please select a Google Map Field for this type'));
      }
    }
  }
}
