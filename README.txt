
CONTENTS OF THIS FILE
---------------------

  * Description and Features
  * Installation
  * Configuration


Description and Features
------------
This module allows the user to configure a block to show locations mapped
using the google map field block. The module allows the user to configure
what content types (The content type needs to have the google map field to
be configurable) are show on the block. The user can also decide a color
to represent the content type. The color will be the color of the marker on
the map block

The user will need a google map API key top use the module. The user can
control the width and the height of the map block.

All default Google map functions also will be available on the block


INSTALLATION
------------

Install just as you install a usual contributed module
see http://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------

Config options are very simple and straight forward. Found in
admin/config/settings/google_map_field_block
